public class Main {
	
	public Main() {
		System.out.println("Dziala konstruktor klasy main");
	}
	
	private long getFactoral(final long digit)
	{
			//Zaimplementowac algorytm wyliczania silni
			//prosze dodac zabezpieczenie przed wprowadzeniem ujemnej liczby:
			//w takim przypadku prosze zwrocic null
			if(digit < 1)
				return 1;
			else
				return digit * getFactoral(digit-1);
	}
	
	public static void main(String[] args) {
		long digit = 5;
		
		Main main = new Main();
		long factoral = main.getFactoral(digit);
		System.out.printf("Silnia(%d) = %d \n", digit, factoral);
	}
}